;; -*- eval: (outline-hide-sublevels 6) -*-

;;; Packages

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;; (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-hook 'dired-load-hook (lambda () (load "dired-x")))
(package-initialize)

;;; Custom

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-revert-interval 1)
 '(auto-save-file-name-transforms (quote ((".*" "~/.emacs.d/backup/" t))))
 '(backup-by-copying t)
 '(backup-directory-alist (quote (("." . "~/.emacs.d/backup/"))))
 '(browse-url-browser-function (quote browse-url-firefox))
 '(column-number-mode t)
 '(custom-buffer-done-kill t)
 '(custom-enabled-themes (quote (doom-tomorrow-night)))
 '(custom-safe-themes
   (quote
    ("8aca557e9a17174d8f847fb02870cb2bb67f3b6e808e46c0e54a44e3e18e1020" default)))
 '(delete-old-versions t)
 '(dired-dwim-target t)
 '(dired-listing-switches "-lh")
 '(electric-pair-mode t)
 '(elpy-get-info-from-shell t)
 '(elpy-remove-modeline-lighter t)
 '(elpy-shell-echo-input-lines-head 3)
 '(elpy-shell-echo-input-lines-tail 3)
 '(elpy-syntax-check-command "flake8 --ignore E306,W504")
 '(erc-nick "mmplx")
 '(erc-nick-uniquifier "_")
 '(fill-column 80)
 '(fit-window-to-buffer-horizontally t)
 '(flymake-error-bitmap (quote (exclamation-mark compilation-error)))
 '(global-company-mode t)
 '(gnus-article-sort-functions (quote ((not gnus-article-sort-by-date))))
 '(gnus-auto-select-first t)
 '(gnus-directory "~/.emacs.d/news")
 '(gnus-newsgroup-maximum-articles 150)
 '(gnus-secondary-select-methods (quote ((nntp "news.gmane.org"))))
 '(gnus-select-method
   (quote
    (nnimap "gmail"
            (nnimap-address "imap.gmail.com")
            (nnimap-server-port "imaps")
            (nnimap-stream ssl))))
 '(gnus-thread-sort-functions (quote (gnus-thread-sort-by-most-recent-date)))
 '(grep-command "rg --color always -nH --null -e ")
 '(grep-find-command
   (quote
    ("find . -type f -exec rg --color always -nH --null -e  \\{\\} +" . 49)))
 '(gud-pdb-command-name "python -m pdb")
 '(ido-default-buffer-method (quote selected-window))
 '(ido-enable-flex-matching t)
 '(ido-everywhere t)
 '(ido-mode (quote both) nil (ido))
 '(ido-ubiquitous-mode t)
 '(ido-use-virtual-buffers t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(ispell-program-name "/usr/bin/hunspell")
 '(markdown-command "markdown_py -x extra")
 '(markdown-fontify-code-blocks-natively t)
 '(markdown-hide-urls t)
 '(markdown-split-window-direction (quote right))
 '(menu-bar-mode nil)
 '(message-directory "~/.emacs.d/mail")
 '(mm-discouraged-alternatives (quote ("text/html" "text/richtext")))
 '(org-agenda-todo-ignore-scheduled (quote future))
 '(org-babel-load-languages (quote ((emacs-lisp . t) (python . t) (latex . t))))
 '(org-babel-python-command "ipython -i --simple-prompt")
 '(org-beamer-environments-extra
   (quote
    (("proposition" "P" "\\begin{proposition}%a[%h]" "\\end{proposition}"))))
 '(org-confirm-babel-evaluate nil)
 '(org-cycle-separator-lines 1)
 '(org-enforce-todo-dependencies t)
 '(org-export-backends (quote (ascii beamer html latex md)))
 '(org-export-with-smart-quotes t)
 '(org-footnote-auto-adjust t)
 '(org-highlight-latex-and-related (quote (native)))
 '(org-latex-image-default-width ".6\\linewidth")
 '(org-latex-packages-alist (quote (("" "custom" t))))
 '(org-latex-pdf-process
   (quote
    ("time latexmk -outdir=/tmp/latexmk -f -pdf %F; mv %f /tmp/latexmk; mv /tmp/latexmk/%b.pdf %o")))
 '(org-noter-always-create-frame nil)
 '(org-outline-path-complete-in-steps nil)
 '(org-preview-latex-default-process (quote dvipng))
 '(org-preview-latex-image-directory "/tmp/ltximg/")
 '(org-refile-use-outline-path (quote file))
 '(org-use-fast-tag-selection t)
 '(package-selected-packages
   (quote
    (cython-mode ess treemacs-projectile treemacs yasnippet-snippets org-noter pdf-tools projectile visual-fill-column magit xclip ido-completing-read+ markdown-mode elpy doom-themes)))
 '(pdf-view-midnight-colors (quote ("#c5c8c6" . "#1d1f21")))
 '(pdf-view-resize-factor 1.07)
 '(projectile-dynamic-mode-line nil)
 '(projectile-generic-command "fd --type file --print0")
 '(python-shell-interpreter "ipython")
 '(python-shell-interpreter-args "-i --simple-prompt")
 '(ring-bell-function (quote doom-themes-visual-bell-fn))
 '(safe-local-variable-values (quote ((eval outline-hide-sublevels 6))))
 '(send-mail-function (quote smtpmail-send-it))
 '(set-mark-command-repeat-pop t)
 '(show-paren-mode t)
 '(shr-color-visible-luminance-min 68)
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 25)
 '(split-width-threshold 150)
 '(sql-interactive-mode-hook (quote (toggle-truncate-lines)))
 '(sql-postgres-options (quote ("-P" "pager=off" "-e")))
 '(tab-width 4)
 '(text-scale-mode-step 1.05)
 '(tooltip-frame-parameters
   (quote
    ((name . "tooltip")
     (internal-border-width . 5)
     (border-width . 1)
     (no-special-glyphs . t)
     (font-backend . "xft"))))
 '(truncate-partial-width-windows t)
 '(use-dialog-box nil)
 '(user-full-name "Carlos Pita")
 '(user-mail-address "carlosjosepita@gmail.com")
 '(version-control t)
 '(winner-mode t)
 '(x-gtk-use-system-tooltips nil)
 '(xclip-mode t))

;;; Theme

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-quoted ((t (:slant italic))))
 '(company-tooltip ((t (:inherit default))))
 '(custom-state ((t (:background "#1d1f21"))))
 '(doom-modeline-error ((t (:background "#cc6666" :foreground "#1d1f21"))))
 '(erc-current-nick-face ((t (:foreground "#b5bd68" :weight bold))))
 '(erc-timestamp-face ((t (:foreground "#8abeb7"))))
 '(fringe ((t (:background "#1c1e20"))))
 '(line-number ((t (:height 0.85))))
 '(line-number-current-line ((t (:height 0.85))))
 '(mode-line ((t (:foreground "#cfd7d3" :background "#13191c"))))
 '(mode-line-inactive ((t (:foreground "#83898b" :background "#13191c"))))
 '(org-block ((t (:background "#1d1f21"))))
 '(org-block-begin-line ((t (:foreground "#5a5b5a" :background "#1d1f21"))))
 '(org-latex-and-related ((t (:foreground "#b294bb"))))
 '(region ((t (:background "#282a2e"))))
 '(secondary-selection ((t (:background "#414241"))))
 '(tooltip ((t (:background "#292b2b" :foreground "#c5c8c6" :height 100 :family "DejaVu Sans"))))
 '(vertical-border ((t (:foreground "#101619" :background "#101619"))))
 '(widget-field ((t (:background "#282a2e")))))

(add-hook 'Info-mode-hook
          (lambda ()
            (face-remap-set-base 'default :family "DejaVu Sans Mono")))

(doom-themes-org-config)

;;; Common

;;;; Utilities

(defun my-read-file (path)
  (with-temp-buffer
    (insert-file-contents path)
    (buffer-string)))

;;;; Base modes

(require 'whitespace)
(require 'hideshow)
(require 'outline)

(defun my-common-init ()
  (when (and buffer-file-name (not buffer-read-only))
    (setq-local whitespace-style '(face trailing empty))
    (my-fill-mode)
    (whitespace-mode)))

(defun my-prog-mode-hook ()
  (my-common-init)
  (flymake-mode)
  (hs-minor-mode)
  (outline-minor-mode))
(add-hook 'prog-mode-hook #'my-prog-mode-hook)

(defun my-text-mode-hook()
  (my-common-init))
(add-hook 'text-mode-hook #'my-text-mode-hook)

;;;; Global keys

(defun my-simplify-fold-keymap (mode)
  (let ((map (make-sparse-keymap)))
     (map-keymap (lambda (ev def)
                   (define-key map (string (event-basic-type ev)) def))
                 (lookup-key
                  (alist-get mode minor-mode-map-alist)
                  (kbd "C-c @")))
     map))

(defvar my-keymap (make-sparse-keymap))
(dolist (bind `(("b" . windmove-left)
                ("c" . ,(make-sparse-keymap))
                ("e" . flymake-goto-next-error)
                ("E" . flymake-goto-prev-error)
                ("f" . windmove-right)
                ("g" . magit-status)
                ("i" . imenu)
                ("m" . ,(make-sparse-keymap))
                ("m f" . flymake-mode)
                ("m n" . display-line-numbers-mode)
                ("m r" . auto-revert-mode)
                ("m s" . flyspell-mode)
                ("m w" . my-fill-mode)
                ("n" . windmove-down)
                ("o" . (lambda () (interactive)
                         (switch-to-buffer (other-buffer))))
                ("O" . recentf-open-files)
                ("p" . windmove-up)
                ("r" . revert-buffer)
                ("s" . ,(my-simplify-fold-keymap #'hs-minor-mode))
                ("s s" . hs-toggle-hiding)
                ("t" . treemacs)
                ("T" . treemacs-select-window)
                ("u" . my-increment-at-point)
                ("w" . (lambda () (interactive)
                         (setq browse-url-new-window-flag t)))
                ("x" . ,(make-sparse-keymap))
                ("x p" . pyvenv-activate)
                ("y" . yas-insert-snippet)
                ("z" . ,(my-simplify-fold-keymap #'outline-minor-mode))
                ("z z" . outline-toggle-children)))
  (define-key my-keymap (kbd (car bind)) (cdr bind)))
(global-set-key (kbd "C-c") my-keymap)

;;;; Whitespace

(defun my-whitespace-set (style on)
  (setq whitespace-style (if (memq style whitespace-style)
                             (unless on (delq style whitespace-style))
                           (when on (cons style whitespace-style))))
  (when (and (symbolp 'whitespace-mode) whitespace-mode)
    (whitespace-mode 0)
    (whitespace-mode 1)))

;;;; Line wrapping

(define-minor-mode my-fill-mode nil nil nil nil
  (my-whitespace-set 'lines-tail my-fill-mode)
  (visual-line-mode (when my-fill-mode -1))
  (visual-fill-column-mode (when my-fill-mode -1))
  (auto-fill-mode (unless my-fill-mode -1)))

;; https://github.com/joostkremers/visual-fill-column#adjusting-text-size
(advice-add #'text-scale-adjust :after #'visual-fill-column-adjust)

;;;; Line numbering

(defun my-goto-line-numbers-advice (fun)
  (interactive)
  (unwind-protect
      (progn (display-line-numbers-mode 1)
             (call-interactively fun))
    (display-line-numbers-mode -1)))
(advice-add #'goto-line :around #'my-goto-line-numbers-advice)

;;;; Projectile

(require 'projectile)
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c j") 'projectile-command-map)

;;;; Company

(global-set-key (kbd "C-<tab>") #'company-complete)

(eval-after-load 'company
  '(progn
     (define-key company-active-map (kbd "<tab>")
       'company-complete-common-or-cycle)
     (define-key company-active-map (kbd "<backtab>")
       'company-select-previous)
     (define-key company-active-map (kbd "M-<tab>")
       'company-complete)))

;;;; Modeline

(dolist (mode '(projectile-mode
                whitespace-mode
                hs-minor-mode
                outline-minor-mode
                auto-fill-function))
  (setcar (alist-get mode minor-mode-alist) ""))

(let ((mode (assq #'flymake-mode minor-mode-alist)))
  (setq minor-mode-alist (cons mode (remq mode minor-mode-alist))))

(defun my-flymake-modeline-advice (ret)
  (setf (cadar ret) " Fly") ret)
(advice-add #'flymake--mode-line-format
            :filter-return #'my-flymake-modeline-advice)

;;;; Misc

(yas-global-mode 1)
(recentf-mode 1)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(setq frame-title-format "%b - Emacs")
(setq calc-gnuplot-default-device "wxt")

;; https://github.com/bbatsov/projectile/issues/1387
;; Also needed for treemacs, so I hacked it at a lower level.
(defun my-dbox-dir-advice (dir)
  (replace-regexp-in-string "apps/Dropbox/?" "dbox/" dir))
(advice-add #'file-truename :filter-return #'my-dbox-dir-advice)

(defun my-increment-at-point (&optional arg)
  (interactive "P")
  (skip-chars-backward "0-9")
  (when (looking-at " *[0-9]+")
    (replace-match (format (format "%%%sd" (length (match-string 0)))
                           (+ (or arg 1) (string-to-number (match-string 0)))))))

(defun my-browse-url-advice (&rest _args)
  (setq browse-url-new-window-flag nil))
(advice-add #'browse-url :after #'my-browse-url-advice)

;;; Major modes

;;;; Emacs lisp

(defun my-emacs-lisp-mode-hook ()
  (setq tab-width 8))
(add-to-list 'emacs-lisp-mode-hook #'my-emacs-lisp-mode-hook)

;;;; Elpy

(defun my-elpy-mode-hook ()
  (setq fill-column 79
        ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=32120
        tab-width 4))
(add-hook 'elpy-mode-hook #'my-elpy-mode-hook)

;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=32344
;; https://github.com/jorgenschaefer/elpy/issues/1428
(defun my-elpy-highlight-input-advice
    (f string face &optional no-font-lock)
  (if (not (eq face 'comint-highlight-input))
      (funcall f string face no-font-lock)
    (funcall f string face t)
    (python-shell-font-lock-post-command-hook)))
(advice-add #'elpy-shell--insert-and-font-lock
            :around #'my-elpy-highlight-input-advice)

(defun my-comint-highlight-input-advice (f &rest args)
  (if (eq major-mode 'inferior-python-mode)
      (cl-letf* ((g (symbol-function 'add-text-properties))
                 ((symbol-function 'add-text-properties)
                  (lambda (start end properties &optional object)
                    (unless (eq (nth 3 properties) 'comint-highlight-input)
                     (funcall g start end properties object)))))
        (apply f args))
    (apply f args)))
(advice-add #'comint-send-input :around #'my-comint-highlight-input-advice)

(elpy-enable)

;; https://github.com/jorgenschaefer/elpy/issues/1399
(define-key elpy-mode-map (kbd "M-<tab>") #'company-capf)

;;;; Shell

(add-to-list 'auto-mode-alist '("PKGBUILD" . sh-mode))

(defun my-shell-mode-hook ()
  (face-remap-set-base 'comint-highlight-prompt :inherit nil))
(add-hook 'shell-mode-hook #'my-shell-mode-hook)

;;;; Org

;;;;; Imports

(require 'org-protocol)
(require 'org-capture)
(require 'org-agenda)
(require 'org-tempo)
(require 'ox)

;;;;; Keys

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c k") 'org-capture)

(setq my-org-keys `(("<tab>" . company-complete)
                    ("i" . latex-insert-block)
                    ("p" . my-org-latex-toggle-previewer)
                    ("\\" . ,(kbd "C-b C-f"))
                    ("e" . ,(kbd "C-x b org SPC output RET \
                                  M-< C-s Fatal SPC error"))))
;;;;; Notes & GTD

(setq org-directory "~/dbox/gtd"; Used by org-capture-templates
      org-capture-templates
      `(("i" "Inbox Note" entry (file "inbox.org")
         "* %?" :empty-lines 1)
        ("p" "Protocol Link" entry (file "inbox.org")
         "* %:description\n\n%:link\n\n%i%?" :empty-lines 1)
        ("L" "Protocol Link" entry (file "inbox.org")
         "* %:description\n\n%:link%?" :empty-lines 1))
      org-tag-alist '(("TARGET" . ?T))
      org-tag-faces '(("PROJECT" . (:inherit 'org-todo)))
      org-tags-exclude-from-inheritance '("PROJECT" "TARGET")
      org-todo-keywords '((type "NEXT" "WAITING" "|" "DONE" "CANCELLED"))
      org-todo-keyword-faces '(("WAITING" . (:foreground "#B294BB")))
      org-default-priority ?C
      org-priority-start-cycle-with-default nil
      org-stuck-projects '("+PROJECT/-DONE" ("NEXT" "WAITING") nil "")
      org-agenda-files (list org-directory)
      org-agenda-sorting-strategy
      (cons '(todo tag-up priority-down) org-agenda-sorting-strategy)
      org-agenda-custom-commands
      '(("c" "List TODO entries w/breadcrumb" alltodo ""
         ((org-agenda-prefix-format "\n %b\n    ")))))

(let ((targets `("~/dbox/notes/notes.org"
                 "~/dbox/notes/journal.org"
                 ,(concat org-directory "/gtd.org"))))
  (setq org-refile-targets `((,(concat org-directory "/anki.org") :level . 0)
                             (,targets :maxlevel . 1)
                             (,targets :tag . "PROJECT")
                             (,targets :tag . "TARGET")
                             (nil :tag . "TARGET"))))

;; https://emacs.stackexchange.com/a/47803/19894
(defun my-org-gtd-advice (f &rest args)
  (if (and buffer-file-name
           (file-in-directory-p buffer-file-name org-directory))
    (let ((org-tag-alist
           (org--tag-add-to-alist
            '(("@compras" . ?c) ("@trabajo" . ?t) ("PROJECT" . ?P))
            (default-value 'org-tag-alist))))
      (apply f args))
    (apply f args)))
(advice-add #'org-set-regexps-and-options :around #'my-org-gtd-advice)

;;;;; Links

(defun my-org-find-in-docs (tag)
  (concat "file:/"
          (substring (shell-command-to-string
                      (concat "find ~/docs/ -iname '*" tag "*' | head -1"))
                     0 -1)))

(setq org-link-abbrev-alist
      '(("google" . "https://www.google.com/#q=")
        ("doc" . my-org-find-in-docs)))

(defun my-org-ordinal-advice (fun element &rest args)
  (or (org-element-property :my-ordinal element)
      (and (eq (org-element-type element) 'target)
           (org-element-property
            :my-ordinal (org-element-lineage element '(special-block))))
      (apply fun (cons element args))))
(advice-add #'org-export-get-ordinal :around #'my-org-ordinal-advice)

;;;;; Looks

(font-lock-add-keywords
 'org-mode `((,(concat "\\(#\\+\\(BEGIN\\|begin\\|END\\|end\\)_"
                       "\\(\\S-+\\)\\)\\(\\(: \\|[^\n:]\\)*\\)")
              (1 '(:foreground "#5a5b5a" :background "#292b2b") t) ; directive
              (3 '(:foreground "#81a2be" :background "#292b2b") t) ; kind
              (4 '(:foreground "#c5c8c6") t))) ; title
 t)

(setq org-html-head "<style type=\"text/css\">
  body { max-width: 800px; margin-left: 30px; }
</style>")

;;;;; Babel

(setq org-babel-default-header-args:python '((:session . "Org Python"))
      org-babel-default-header-args:latex '((:results . "file raw")
                                            (:exports . "results")
                                            (:cache . "yes")
                                            (:headers . ("\\usepackage{tikz}"))
                                            (:imagemagick . "yes")
                                            (:fit . "yes")))

(defun my-org-babel-redisplay-images ()
  (when org-inline-image-overlays
    (org-redisplay-inline-images)))
(add-hook 'org-babel-after-execute-hook #'my-org-babel-redisplay-images)

(org-babel-lob-ingest "~/lib/org/babel.org")

;;;;; Maths

(setq org-format-latex-options
      (plist-put org-format-latex-options :scale 1.3))

(add-to-list 'org-html-mathjax-options
             '(path "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_CHTML"))

(push '(:with-math-html nil "math-html" 'unset)
      (org-export-backend-options (org-export-get-backend 'html)))

;; https://www.reddit.com/r/emacs/comments/9ggimh
(defun my-org-latex-preview-advice (beg end &rest _args)
    (let* ((ov (car (overlays-in beg end)))
           (img (cdr (overlay-get ov 'display)))
           (new-img (plist-put img :ascent 90)))
      (overlay-put ov 'display (cons 'image new-img))))
(advice-add #'org--format-latex-make-overlay
            :after #'my-org-latex-preview-advice)

(defun my-org-latex-toggle-previewer ()
  (interactive)
  (setq org-preview-latex-default-process
        (if (eq org-preview-latex-default-process 'dvipng)
            'imagemagick 'dvipng)))

(setq my-org-theorems
      '(("theorem" . ?t) ("proposition" . ?p)
        ("corollary" . ?c) ("definition" . ?d)))
(dolist (theorem my-org-theorems)
  (add-to-list 'org-structure-template-alist
               (cons (format "t%c" (cdr theorem)) (car theorem))))
(add-to-list 'org-structure-template-alist
             '("p" . "proof")
             '("x" . "alertblock {\\faExclamationTriangle{}}"))

(defun my-org-math-html (mathjax) (concat (when mathjax (concat
"<div style=\"display:none\">
\\[\n" (my-read-file "~/lib/tex/latex/custom-common.tex") "\\]
</div>\n\n"))
"<style type=\"text/css\">
.theorem { background-color: #f5f5f5; font-style: italic; display: block; }
.theorem .name, .theorem .number, .theorem .title {
  font-weight: bold; font-style: normal; }
.proof { display: block; }
.proof .name { font-style: italic; }
</style>\n\n"))

;;;;; Export

(defun my-org-preprocess-hook (backend)
  (let ((info (org-export-get-environment)))
    (while (re-search-forward "{{{\\(:[[:alnum:]-]+\\)}}}" nil t)
      (let ((val (plist-get info (intern (match-string 1)))))
        (when val (replace-match (car val)))))))
(add-hook 'org-export-before-parsing-hook #'my-org-preprocess-hook)

(defun my-org-transform-filter (tree backend info)
  (let ((ordinal 0)
        (attr (lambda (elem name value)
                (org-element-put-property
                 elem name (cons value (org-element-property name elem)))))
        (first (lambda (elem) (car (org-element-contents elem))))
        (theorem (lambda (name arg)
                   (org-element-create 'keyword `(:key "HTML" :value ,(concat
                    (format "<span class=\"name\">%s</span>"
                            (capitalize (string-trim-right name "\\*")))
                    (unless (string-match-p "\\*$" name)
                      (setq ordinal (1+ ordinal))
                      (org-element-put-property block :my-ordinal ordinal)
                      (format "&nbsp;<span class=\"number\" id=\"%s\">%s</span>"
                              (org-export-get-reference block info)
                              (number-to-string ordinal)))
                    (when (org-string-nw-p arg)
                      (format "&nbsp;<span class=\"title\">(%s)</span>"
                              arg)))))))
        (proof (org-element-create 'export-snippet '(:back-end "html" :value
                "<span class=\"name\">Proof.&nbsp;</span>")))
        (qed (org-element-create 'export-snippet '(:back-end "html" :value
              "&nbsp;&#8718;"))))
    (org-element-map tree 'special-block
      (lambda (block)
        (goto-char (org-element-property :post-affiliated block))
        (looking-at "[ \t]*#\\+begin_\\S-+[ \t]*\\([^\n]*\\)")
        (let ((name (org-element-property :type block))
              (arg (match-string-no-properties 1)))
          (cond ((org-export-derived-backend-p backend 'latex)
                 (when (org-string-nw-p arg)
                   (funcall attr block :attr_latex
                            (if (string-match-p "^[[{]" arg)
                                (concat ":options " arg)
                              (format ":options [%s]" arg)))))
                ((org-export-derived-backend-p backend 'html)
                 (cond ((assoc (string-trim-right name "\\*") my-org-theorems)
                        (funcall attr block :attr_html ":class theorem")
                        (when (org-export-derived-backend-p backend 'html)
                          (funcall attr block :attr_html ":markdown true"))
                        (org-element-insert-before
                         (funcall theorem name arg) (funcall first block)))
                       ((string= name "proof")
                        (org-element-insert-before
                         proof (funcall first (funcall first block)))
                        (org-element-adopt-elements
                         (car (last (org-element-contents block))) qed))))))
        nil))))
(add-to-list 'org-export-filter-parse-tree-functions #'my-org-transform-filter)

(defun my-org-postprocess-filter (body backend info)
  (when (and (if (eq (plist-get info :with-math-html) 'unset)
                 (and (not (org-export-derived-backend-p backend 'md))
                      (not (memq 'body-only (plist-get info :export-options))))
               (plist-get info :with-math-html))
             (org-element-map (plist-get info :parse-tree)
                 '(latex-fragment latex-environment) #'identity info t nil t))
    (let ((html (my-org-math-html (memq (plist-get info :with-latex)
                                        '(t mathjax)))))
      (replace-regexp-in-string "\\`\\(---\\(.\\|\n\\)*?\n---\n\n\\)?"
                                (lambda (m) (concat m html)) body nil t))))
(add-to-list 'org-export-filter-body-functions #'my-org-postprocess-filter)

;;;;; Macros

(add-to-list 'org-export-global-macros
             '("if-latex-else" . "(eval (if (org-export-derived-backend-p org-export-current-backend 'latex) \"$1\" \"$2\"))"))
;; https://emacs.stackexchange.com/questions/7792/7793#7793
(add-to-list 'org-export-global-macros
             '("bib" . "[[bib:$1][@@latex:\\mbox{\\char91@@@@html:&#91;@@$1@@latex:\\char93}@@@@html:&#93;@@]]"))
(add-to-list 'org-export-global-macros '("bibdef" . "<<bib:$1>> ~[$1]~"))

;;;;; Outline

(setq org-hide-leading-stars nil) ; Revert doom theme setting

(defun my-org-light-outline ()
  (setq-local org-level-color-stars-only t)
  (face-remap-add-relative 'org-level-1 :background "#1d1f21")
  (org-indent-mode)
  (my-fill-mode -1)
  (my-whitespace-set 'empty nil))

(add-to-list 'org-startup-options '("light" my-org-startup-light t))
(add-to-list 'org-startup-options '("nolight" my-org-startup-light nil))

;;;;; Hook

(defun my-org-mode-hook ()
  (dolist (bind my-org-keys)
    (local-set-key (kbd (concat "C-c c " (car bind))) (cdr bind)))
  ;; https://www.reddit.com/r/emacs/comments/9ktjx3
  (my-whitespace-set 'lines-tail nil)
  (let ((dbox (and buffer-file-name
                   (file-in-directory-p buffer-file-name "~/dbox"))))
    (when dbox (auto-revert-mode))
    (when (if (boundp 'my-org-startup-light) my-org-startup-light dbox)
      (my-org-light-outline)))
  (add-hook 'completion-at-point-functions
            #'pcomplete-completions-at-point nil t))
(add-hook 'org-mode-hook #'my-org-mode-hook)

;;;;; Misc

(add-to-list 'org-file-apps
             '("\\.x?html?\\'" . (lambda (path _link) (browse-url path))))

;;;; Markdown

(defun my-markdown-mode-hook ()
  (when (string-suffix-p "README.md" buffer-file-name)
    (my-fill-mode -1)
    ;; https://www.reddit.com/r/emacs/comments/9ktjx3
    (my-whitespace-set 'lines-tail nil)))
(add-hook 'markdown-mode-hook #'my-markdown-mode-hook)

;;;; Pdf-tools

(pdf-tools-install)

(add-hook 'pdf-view-mode-hook #'pdf-view-midnight-minor-mode)

(defun my-lightweight-outline ()
  (dolist (face '(outline-1 outline-2 outline-3 outline-4 outline-5))
    (face-remap-add-relative face :inherit 'default)))
(add-hook 'pdf-outline-buffer-mode-hook #'my-lightweight-outline)
