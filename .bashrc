#!/bin/bash

. ~/lib/bash/util.sh

export HISTCONTROL=ignoreboth:erasedups

shopt -s checkwinsize
shopt -s autocd

alias e='$EDITOR'
alias ne='emacsclient -n'
alias g='git'
alias l='less -R'
alias cp='cp -i'
alias ls='ls --color=auto'
alias man='COLUMNS=80 man'
alias yay='yay --removemake'
alias sc='TERM=xterm sudo systemctl'

o() { xdg-open "$1" &> /dev/null; }
se() { $EDITOR /sudo::$1; }

# Devices

bt() {
    local device=$(bluetoothctl devices |
                       fzf -q "$1" -1 --with-nth=3.. | cut -d ' ' -f 2)
    [[ $device ]] && bluetoothctl connect "$device"
}

btoff() {
    bluetoothctl info | grep -q '^Device' && bluetoothctl disconnect
}

net() {
    [[ $1 == '!' ]] && { connmanctl scan wifi; shift; }
    local service=$(connmanctl services | grep -v 'hidden' |
                        fzf -q "$1" -1 --with-nth=..-2 | sed 's/.* //')
    [[ $service ]] && connmanctl connect "$service"
}

netoff() {
    local service=$(connmanctl services | grep -E '^..(O|R)' | sed 's/.* //')
    [[ $service ]] && connmanctl disconnect "$service"
}

tpad() {
    synclient TouchpadOff=$(synclient -l | grep -c 'TouchpadOff.*=.*0')
}

# Storages

alias rclone='rclone -v --drive-use-trash'
alias rsync='rsync -auP'
alias backup='rsync --exclude={.cache,down} ~/. ~/mnt/a73f174e-2a33-4a49-b105-8730ccce73a0/backup'
alias gparted='sudo sh -c "GTK2_RC_FILES=$HOME/.gtkrc-2.0 gparted"'
alias sshfs='sshfs -oauto_cache,cache_timeout=120,reconnect,no_readahead'

mnt() {
    if [[ $1 && -d ~/mnt/$1 ]]; then
        local name=$1; shift
    else
        local name=$(ls -1 ~/mnt | grep -v media | fzf)
    fi
    case $name in
        *drive) gcsf mount ~/mnt/$name -s $name &> /tmp/gcsf-$name.log & ;;
        phone|tablet) curlftpfs 192.168.1.$1:9999 mnt/$name ;;
    esac
}

umnt() {
    fusermount -u ~/mnt/$(ls -1 ~/mnt | grep -v media | fzf -q "$1" -1)
}

# Orgzly

orgzly() {
    local orig=$(fd -e org -E orgzly -t f . ~/dbox/{notes,projs} | fzf)
    [[ $orig ]] || return
    local dest=~/dbox/orgzly/"$(basename "$orig")"
    mv "$orig" "$dest" && \
        dropbox-cli exclude add "$(realpath "$orig")" && \
        ln -s "$dest" "$orig"
}

unorgzly() {
    local orig=$(fd -e org -E orgzly -t l . ~/dbox/{notes,projs}| fzf -0)
    [[ $orig ]] || return
    mv "$(realpath "$orig")" "$orig" && \
        dropbox-cli exclude remove $(realpath "$orig")
}

# Python

alias py='python -i -c "from math import *"'

pyenv() {
    local path=${1:-venv}
    [[ -d $path ]] || python -m venv "$path"
    source $path/bin/activate
}

pyupgrade() {
    [[ $VIRTUAL_ENV ]] && local local='--local' || local user='--user'
    pip install --upgrade $user $(pip list --outdated $user$local | \
                                      tail -n +3 | awk '{print $1}')
}

# Packages

alias rmorphs='sudo pacman -Rs `pacman -Qdtq`'

# https://unix.stackexchange.com/a/409903/329839 (see my comment)
lsinst() {
    comm -23 <(pacman -Qqe | sort) \
             <(pacman -Qqg base -g base-devel -g xorg | sort | uniq)
}

modcfg() {
    find ~ -path ~/mnt -prune -o -mmin -${1:-90} -path $HOME'/.*' | \
        grep -v -e '.mozilla' -e '.cache' -e '\.git' | less
}

# Prompt

_ps1_branch() {
    local repo=$(git rev-parse --show-toplevel 2> /dev/null)
    [[ $repo && $repo != $HOME ]] && {
        printf "$(git branch | sed -nr 's/^\* (.*)/\1/p')"
    }
}
_short_path() {
    echo ${PWD/$HOME/'~'} | sed -r 's:([^/]{,3})[^/]*/:\1/:g'
}
PS1='\[\e[1;32m\]$(_short_path)\[\e[1;37m\]:\[\e[1;31m\]$(_ps1_branch)\[\e[1;37m\]:\[\e[0m\] '

################################# Inside Emacs #################################

if [[ $INSIDE_EMACS == *comint ]]; then
    export TERM=vt100
    alias git='git --no-pager'
    return
fi

################################# Outside Emacs ################################

export TERM=xterm-24bit
export EDITOR='emacsclient -t'
PROMPT_COMMAND='echo -ne "\033]0;$(_short_path | sed 's:^~$:'$HOME':')\007"'

stty -ixon

# Completion

. /usr/share/bash-completion/bash_completion
_completion_loader systemctl; complete -F _systemctl sc

. /usr/share/git/completion/git-completion.bash
__git_complete g _git

# z & fzf

export FZF_DEFAULT_COMMAND="fd --type file --type directory"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd --type directory"
export FZF_DEFAULT_OPTS='
    --bind alt-enter:print-query
    --height=25% --reverse
    --color=bg+:#282a2e,bg:#1d1f21,spinner:#8abeb7,hl:#81a2be
    --color=fg:#b4b7b4,header:#81a2be,info:#f0c674,pointer:#8abeb7
    --color=marker:#8abeb7,fg+:#e0e0e0,prompt:#f0c674,hl+:#81a2be'
export _Z_NO_RESOLVE_SYMLINKS=1

. /usr/share/z/z.sh # This needs to be below git completion to work (why???)
. /usr/share/fzf/key-bindings.bash
. /usr/share/fzf/completion.bash

complete -F _fzf_path_completion e se

zcd() {
    local dir=$(z | tac | fzf --with-nth=2 | sed 's/.* //')
    [[ $dir ]] && cd "$dir"
}

# https://github.com/jupyter/notebook/issues/4585
__jupyter_complete_baseopts=" "
