import os
import warnings


warnings.filterwarnings('ignore')

c = get_config()  # noqa

c.TerminalIPythonApp.extensions = [
    'autoreload'
]
c.TerminalIPythonApp.exec_lines = [
    '%autoreload 2',
    'import numpy as np',
    'import scipy as sp',
    'import pandas as pd',
    'import matplotlib.pyplot as plt',
    'import seaborn as sns',
    'import statsmodels.api as sm',
    'import statsmodels.formula.api as smf',
    'import util as ut',
    'import sci'
]
c.InteractiveShell.colors = 'nocolor'
c.InteractiveShellApp.matplotlib = 'gtk3'
os.environ['GTK_THEME'] = 'Adwaita:light'
