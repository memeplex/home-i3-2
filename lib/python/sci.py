"""Miscellanous utilities that depend on the scientific python stack"""

import tempfile
import matplotlib.pyplot as plt
from contextlib import contextmanager


@contextmanager
def org_figure(path=None, size=(3, 2)):
    """Non-interactive plotting context for org babel blocks"""
    itoggle = plt.isinteractive()
    try:
        if itoggle:
            plt.ioff()
        fig = plt.figure(figsize=size)
        yield fig
    finally:
        if path is None:
            path = tempfile.mkstemp(suffix='.png')[1]
        fig.path = path
        plt.savefig(path)
        plt.close()
        if itoggle:
            plt.ion()
