"""Simple lightweight miscellanous utilities"""

import subprocess


def sh(cmd, input=None, capture=True, text=True, check=True):
    if input and not isinstance(input, (bytes, str)):
        input = '\n'.join(input)
    return subprocess.run(cmd, input=input, capture_output=capture, text=text,
                          check=check, shell=True).stdout


def sh_quote(s):
    return "'" + s.replace("'", "'\\''") + "'"
