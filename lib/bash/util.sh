#!/bin/bash

has() {
    stdbuf -o 0 -e 0 "${@:2}" |& tee /dev/tty | grep -P -q "$1"
}

erun() { echo "$@"; "$@"; }
