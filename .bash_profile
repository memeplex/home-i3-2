#!/bin/bash

export EDITOR=emacsclient
export PATH=~/bin:~/local/bin:~/.local/bin${PATH:+:$PATH}
export MANPATH=~/local/share/man:~/.local/share/man:$MANPATH
export LD_LIBRARY_PATH=~/local/lib:~/.local/lib${LD_LIBRARY_PATH:+:LD_LIBRARY_PATH}
export XDG_DATA_DIRS=~/local/share:/usr/local/share:/usr/share
export PYTHONPATH=~/lib/python:~/.local/lib/python3.7/site-packages
export TEXMFHOME=~/lib
export R_LIBS_USER=~/.local/lib/r3.5
export QT_QPA_PLATFORMTHEME='gtk2'
export CALIBRE_USE_SYSTEM_THEME=1

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx &> /tmp/startx.log
fi
